## Python Übungsaufgabe: Objektorientiertes Programmieren mit Dateipersistenz

### Aufgabenstellung
Entwickeln Sie ein Python-Programm, das ein einfaches Buchverwaltungssystem implementiert. Das System sollte folgende Funktionalitäten bieten:

1. **Buch-Objekte erstellen**: Jedes Buch soll durch ein Objekt repräsentiert werden und mindestens Titel, Autor, ISBN-Nummer und Erscheinungsjahr als Attribute haben.

2. **Bibliothek-Objekt**: Erstellen Sie eine Klasse `Bibliothek`, die eine Sammlung von Büchern verwaltet. Diese Klasse sollte Methoden zum Hinzufügen, Entfernen und Suchen von Büchern nach verschiedenen Kriterien (z.B. Titel, Autor) enthalten.

3. **Dateipersistenz**: Die Informationen über die Bücher sollen in einer Datei gespeichert werden. Implementieren Sie Methoden zum Laden der Bücher aus einer Datei beim Start des Programms und zum Speichern der aktuellen Bücherliste in die Datei.

4. **Benutzerinteraktion**: Entwickeln Sie eine einfache Text-basierte Benutzeroberfläche, über die Benutzer Bücher hinzufügen, entfernen, suchen und die gesamte Liste anzeigen können.

### Anforderungen

- **Objektorientierte Programmierung**: Verwenden Sie Klassen und Objekte, um die verschiedenen Komponenten des Systems zu modellieren.
- **Dateiverarbeitung**: Implementieren Sie das Lesen und Schreiben in Dateien, um die Buchdaten zu persistieren.
- **Fehlerbehandlung**: Stellen Sie sicher, dass Ihr Programm angemessen auf Fehleingaben und Fehler bei der Dateiverarbeitung reagiert.
- **Erweiterbarkeit**: Der Code sollte so strukturiert sein, dass zukünftige Erweiterungen, wie das Hinzufügen neuer Attribute für Bücher oder zusätzliche Funktionen für die Bibliothek, leicht umgesetzt werden können.

### Zusätzliche Herausforderungen (optional)

- **Datenformat**: Experimentieren Sie mit verschiedenen Datenformaten für die Speicherung (z.B. JSON, CSV, XML).
- **Erweiterte Suchfunktionen**: Implementieren Sie erweiterte Suchfunktionen, wie die Suche nach Büchern innerhalb eines bestimmten Zeitraums oder nach einer Kombination von Kriterien.
- **Graphische Benutzeroberfläche (GUI)**: Ersetzen Sie die Text-basierte Benutzeroberfläche durch eine GUI, beispielsweise unter Verwendung von Tkinter oder PyQt.
